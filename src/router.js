import Vue from 'vue';
import Vuex from 'vuex'
import Router from 'vue-router';
import store from './store'

Vue.use(Router);

let router = new Router({
  routes: [
  
    {
      path: '/',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/Login/Login.vue')
    },
    {
      path: '/Inicio',
      name: 'inicio',
      
      component: () => import(/* webpackChunkName: "about" */ './views/Inicio/Inicio.vue'),
      meta: {
        requiresAuth: true,
      }
    },

    {
      path: '/Caja',
      name: 'caja',     
      component: () => import(/* webpackChunkName: "about" */ './views/Caja/Caja.vue'),
      meta: {
        requiresAuth: true,
      }
  
    }
  ],

});

router.beforeEach((to, from, next) => {
  if (to.matched.some( (record) => record.meta.requiresAuth)) {
    if (store.getters.authStatus === 'success') {
      next();
      return;
    }
    next('/');
  } else {
    next();
  }
});
export default router;

