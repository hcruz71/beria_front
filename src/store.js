import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    status: '',
    user : '',
    baseUrl: 'http://localhost/',
  },
  mutations: {
    auth_request(state) {
      state.status = 'loading';
    },
    auth_success(state, user) {
      state.status = 'success';
      state.user = user;
    },
    auth_error(state) {
      state.status = 'error';
    },
    auth_sis_error(state) {
      state.status = 'siserror';
    },
    logout(state) {
      state.status = '';
    },   
    base(state) {
      state.baseUrl = 'http://localhost/'
    },
  },
  actions: {
    async login({commit}, loginData){
      const url = 'http://localhost/api_cl/Login/login_usuarios';
      commit('base');
      //const loginData =  {XAPIKEY: 'Ve5muByyKs54wpm62d73lHEwDnXKGiYH1v6G7123',usuariosusr: this.form.usr, usuariospwd: this.form.pwd} ;
  
     const datosAcc = await  axios.post(url, loginData)
      .then(response => {
            
            console.log (response.data.estatus);
            commit('auth_request');
            if (response.data.estatus === "1"){
              commit('auth_success', response.data.registros[0]);
              
            } 
            
            if (response.data.estatus === "0"){
              commit('auth_error');
              
            }

      })
      .catch(e=>{
        commit('auth_sis_error');

     })
     return datosAcc;
     
    },

    logout({commit}){
      commit('logout');
    }

    
    
  },
  getters : {
    authStatus: (state) => state.status,
    infoUsuario: (state) => state.user,
    baseUrl: (state) => state.baseUrl,
  }
})
